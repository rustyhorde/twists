FROM gliderlabs/alpine:latest

ADD target/x86_64-unknown-linux-musl/release/twists /
COPY .env/jasonozias.com.pfx /

EXPOSE 11579
EXPOSE 32276

CMD ["/twists", "-a", "0.0.0.0", "-vv", "--with-tls", "-f", "/jasonozias.com.pfx"] 
