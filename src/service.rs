//! echo service implementation
use base64::decode;
use chrono::{DateTime, Duration, UTC};
use claims::Claims;
use futures::{future, Future};
use slog::Logger;
use std::collections::HashMap;
use std::io;
use tokio_service::Service;
use twist::server::{BaseFrame, OpCode, WebSocketFrame};
use twist::server::{HandshakeRequestFrame, HandshakeResponseFrame};
use twist_jwt::{Algorithm, Header};
use twist_jwt::encode;

/// Generate an `io::ErrorKind::Other` error with the given description.
fn other(desc: &str) -> io::Error {
    io::Error::new(io::ErrorKind::Other, desc)
}

#[derive(Clone)]
/// The `Echo` service struct
pub struct Echo {
    /// An optional slog stdout `Logger`
    stdout: Option<Logger>,
    /// An optional slog stderr `Logger`
    stderr: Option<Logger>,
}

impl Echo {
    /// Add a slog stdout `Logger` to this service.
    pub fn add_stdout(&mut self, stdout: Logger) -> &mut Echo {
        let ps_stdout = stdout.new(o!("module" => module_path!()));
        self.stdout = Some(ps_stdout);
        self
    }

    /// Add a slog stderr `Logger` to this service.
    pub fn add_stderr(&mut self, stderr: Logger) -> &mut Echo {
        let ps_stderr = stderr.new(o!("module" => module_path!()));
        self.stderr = Some(ps_stderr);
        self
    }

    /// Base frame handler.
    fn handle_base_frame(&self, base: &BaseFrame) -> io::Result<WebSocketFrame> {
        let mut ws_frame: WebSocketFrame = Default::default();
        // Echo the frame back to the client
        match base.opcode() {
            OpCode::Text => {
                if let Some(ref stdout) = self.stdout {
                    trace!(stdout, "received text frame")
                }
                let mut text_resp: BaseFrame = Default::default();
                text_resp.set_opcode(OpCode::Text);
                text_resp.set_payload_length(base.payload_length());
                text_resp.set_application_data(base.application_data().clone());
                ws_frame.set_base(text_resp);
            }
            OpCode::Binary => {
                let mut bin_resp: BaseFrame = Default::default();
                bin_resp.set_opcode(OpCode::Binary);
                bin_resp.set_payload_length(base.payload_length());
                bin_resp.set_application_data(base.application_data().clone());
                ws_frame.set_base(bin_resp);
            }
            _ => {
                if let Some(ref stderr) = self.stderr {
                    error!(stderr, "invalid websocket frame received");
                }
                return Err(other("invalid websocket frame received"));
            }
        }
        Ok(ws_frame)
    }

    /// Generate a 401 Unauthorized response.
    fn not_authenticated(&self, resp: &mut HandshakeResponseFrame) {
        let mut www_auth = HashMap::new();
        www_auth.insert("WWW-Authenticate".to_string(),
                        "Twist credentials=<X>".to_string());
        resp.set_code(401);
        resp.set_reason("Unauthorized");
        resp.set_others(www_auth);
    }

    /// authenticate
    fn authenticate(&self,
                    req: &HandshakeRequestFrame,
                    _resp: &mut HandshakeResponseFrame)
                    -> bool {
        let others = req.others();

        if let Some(v) = others.get("Authorization") {
            try_trace!(self.stdout, "Authorization: {}", v);
            let mut encoded = String::new();
            for (idx, chunk) in v.split(' ').enumerate() {
                if (idx == 0 && chunk != "Twist") || idx > 1 {
                    return false;
                } else if idx == 1 {
                    encoded = chunk.to_string();
                }
            }
            match decode(&encoded) {
                Ok(header_val) => {
                    let auth_val = String::from_utf8_lossy(&header_val);
                    let mut username = String::new();
                    let mut password = String::new();
                    for (idx, chunk) in auth_val.split(':').enumerate() {
                        if idx == 0 {
                            username = chunk.to_string();
                        } else if idx == 1 {
                            password = chunk.to_string();
                        } else if idx > 1 {
                            return false;
                        }
                    }

                    if username == "twisty" && password == "twisty" {
                        let mut header: Header = Default::default();
                        header.set_alg(Algorithm::HS512);
                        let mut claims: Claims = Default::default();
                        let now: DateTime<UTC> = UTC::now();
                        match now.checked_add_signed(Duration::minutes(10)) {
                            Some(exp) => claims.exp = exp,
                            None => return false,
                        }
                        match encode::jwt(&header, &claims, b"twist") {
                            Ok(enc) => {
                                try_trace!(self.stdout, "Token: {}", enc);
                                true
                            }
                            Err(e) => {
                                try_error!(self.stderr, "{}", e);
                                false
                            }
                        }
                    } else {
                        false
                    }
                }
                Err(e) => {
                    try_error!(self.stderr, "{}", e);
                    false
                }
            }
        } else {
            false
        }
    }

    /// Add the base response components
    fn add_base_components(&self, req: &HandshakeRequestFrame, resp: &mut HandshakeResponseFrame) {
        resp.set_ws_key(req.ws_key());
        let ext = req.extensions();
        if !ext.is_empty() {
            resp.set_extensions(Some(ext));
        }
        let prot = req.protocol();
        if !prot.is_empty() {
            resp.set_protocol(Some(prot));
        }
    }

    /// Handshake frame handler.
    fn handle_handshake_frame(&self,
                              request: &HandshakeRequestFrame)
                              -> io::Result<WebSocketFrame> {
        try_trace!(self.stdout, "The request\n{}", request);

        let path_parts: Vec<&str> = request.path().split('?').collect();
        let mut path = String::new();
        let mut qs = String::new();
        for (idx, part) in path_parts.iter().enumerate() {
            if idx == 0 {
                path.push_str(*part);
            } else if idx == 1 {
                qs.push_str(*part);
            }
        }

        try_trace!(self.stdout, "path: {}", path);
        try_trace!(self.stdout, "query string: {}", qs);

        let mut frame: WebSocketFrame = Default::default();
        let mut resp: HandshakeResponseFrame = Default::default();

        match &path[..] {
            "/not-found" => {
                resp.set_code(404);
                resp.set_reason("Not Found");
            }
            "/auth" => {
                if self.authenticate(request, &mut resp) {
                    self.add_base_components(request, &mut resp);
                } else {
                    self.not_authenticated(&mut resp);
                }
            }
            _ => {
                self.add_base_components(request, &mut resp);
            }
        }

        try_trace!(self.stdout, "The response\n{}", resp);
        frame.set_serverside_handshake_response(resp);

        Ok(frame)
    }
}

impl Default for Echo {
    fn default() -> Echo {
        Echo {
            stdout: None,
            stderr: None,
        }
    }
}

impl Service for Echo {
    type Request = WebSocketFrame;
    type Response = WebSocketFrame;
    type Error = io::Error;
    type Future = Box<Future<Item = Self::Response, Error = Self::Error>>;

    fn call(&self, req: Self::Request) -> Self::Future {
        if let Some(base) = req.base() {
            match self.handle_base_frame(base) {
                Ok(ws_frame) => future::result(Ok(ws_frame)).boxed(),
                Err(e) => future::err(e).boxed(),
            }
        } else if let Some(request) = req.serverside_handshake_request() {
            match self.handle_handshake_frame(request) {
                Ok(ws_frame) => future::result(Ok(ws_frame)).boxed(),
                Err(e) => future::err(e).boxed(),
            }
        } else {
            try_error!(self.stderr, "invalid websocket frame received");
            future::err(other("invalid websocket frame received")).boxed()
        }
    }
}
