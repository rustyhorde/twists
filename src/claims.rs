//! Claims for JWT
use chrono::{DateTime, UTC};
use twist_jwt::verify;

#[derive(Clone, Deserialize, PartialEq, Serialize)]
/// Twist Claims
pub struct Claims {
    /// Issuer
    pub iss: String,
    /// Subject
    pub sub: String,
    /// Audience
    pub aud: String,
    /// Expiration
    pub exp: DateTime<UTC>,
    /// Not Before
    pub nbf: DateTime<UTC>,
    /// Issued At.
    pub iat: DateTime<UTC>,
    /// JWT Identifier
    pub jti: String,
}

impl Default for Claims {
    fn default() -> Claims {
        Claims {
            iss: "Twist".to_string(),
            sub: String::new(),
            aud: String::new(),
            exp: UTC::now(),
            nbf: UTC::now(),
            iat: UTC::now(),
            jti: String::new(),
        }
    }
}

impl verify::HasRegistered for Claims {
    fn iss(&self) -> Option<String> {
        Some(self.iss.clone())
    }

    fn sub(&self) -> Option<String> {
        Some(self.sub.clone())
    }

    fn aud(&self) -> Option<String> {
        Some(self.aud.clone())
    }

    fn exp(&self) -> Option<DateTime<UTC>> {
        Some(self.exp)
    }

    fn nbf(&self) -> Option<DateTime<UTC>> {
        Some(self.nbf)
    }

    fn iat(&self) -> Option<DateTime<UTC>> {
        Some(self.iat)
    }

    fn jti(&self) -> Option<String> {
        Some(self.jti.clone())
    }
}
