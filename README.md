# twists
Example WebSocket echo server implemented with [twist](https://github.com/rustyhorde/twist) based on [tokio](https://tokio.rs)

## TODO
The base library twist does not **yet** implement all of [RFC 6455](https://tools.ietf.org/html/rfc6455).
For example, protocols and exntension passed in the client header are ignored.   This will come in time.

## Install
Install twists with cargo

`cargo install twists`

## Running
By default twists will listen for incoming connections at 127.0.0.1:11579

`twists`

You can test with the wscat utility

`wscat ws://127.0.0.1:11579 -k 5`

The above will send a ping frame every 5 seconds.  You should see pongs back.  Also text frames will be echoed back.

## Help
Command line help

`twists --help`

## Examples
Enable trace output

`twist -vvv`

Change the unsecure address and port

`twist -a 10.0.0.0 -p 12345`

### TLS Support
twists has support for listen for TLS connections.  To enable, you need a valid .pfx file.  twists uses [native-tls](https://github.com/sfackler/rust-native-tls), and native-tls requires a valid pfx file with password.  By default, twists will run on port 32276.  This can be changed via the tlsport command line argument.


    export PFX_PWD='the password for the pfx file'
    twists -a 10.x.x.x --with-tls -f path/to/pfx


I use fish shell, so

    set -lx PFX_PWD 'the password for the pfx file'
    twists -a 10.x.x.x --with-tls -f path/to/pfx

### Autobahn Test Suite Results
[Results](https://rustyhorde.github.io/twist/autobahn/reports/servers/index.html)

Notes:  The secure results reflect a connection out over the internet and back.  This is due to the hostname requirements of TLS and the need for a valid (read: not self-signed) certificate.
